package facci.pm.chavezzambrano.examensegundoparcial;

public class Estudiante {

    String _id, cedula, ciudad, pais, descripcion, nombre, apellido;

    public Estudiante(String _id, String cedula, String ciudad, String pais, String descripcion, String nombre, String apellido) {
        this._id = _id;
        this.cedula = cedula;
        this.ciudad = ciudad;
        this.pais = pais;
        this.descripcion = descripcion;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public Estudiante() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
}
