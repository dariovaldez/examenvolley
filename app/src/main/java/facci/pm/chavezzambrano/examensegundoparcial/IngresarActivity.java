package facci.pm.chavezzambrano.examensegundoparcial;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;

public class IngresarActivity extends AppCompatActivity {

    EditText txtCedula, txtCiudad, txtPais, txtDescripcion, txtNombre, txtApellido;
    Button btnSubir;

    private RequestQueue mRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar);
        txtCedula = findViewById(R.id.txtCedula);
        txtCiudad = findViewById(R.id.txtciudad);
        txtPais = findViewById(R.id.txtPais);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtNombre = findViewById(R.id.txtNombre);
        txtApellido = findViewById(R.id.txtApellido);
        btnSubir = findViewById(R.id.btnSubir);

        mRequest = Volley.newRequestQueue(getApplicationContext());

        btnSubir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubirDatos();
            }
        });

    }

    private void SubirDatos() {

        String url = "http://backend-posts.herokuapp.com/student";

        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("cedula", txtCedula.getText().toString());
        hashMap.put("ciudad",txtCiudad.getText().toString());
        hashMap.put("pais", txtPais.getText().toString());
        hashMap.put("descripcion", txtDescripcion.getText().toString());
        hashMap.put("nombre", txtNombre.getText().toString());
        hashMap.put("apellido", txtApellido.getText().toString());

        JsonObjectRequest subirDatos = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(hashMap), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("subir", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        mRequest.add(subirDatos);

    }
}
