package facci.pm.chavezzambrano.examensegundoparcial;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ConsultaActivity extends AppCompatActivity implements Response.Listener<JSONArray>, Response.ErrorListener {

    RecyclerView rvUsuarios;
    ArrayList<Estudiante> listEstudiante;

    RequestQueue request;
    JsonArrayRequest jsonArrayRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);

        rvUsuarios = findViewById(R.id.recyclerEstudiantes);
        listEstudiante =new ArrayList<>();

        rvUsuarios.setLayoutManager(new LinearLayoutManager(this.getApplicationContext()));
        rvUsuarios.setHasFixedSize(true);

        request = Volley.newRequestQueue(getApplicationContext());

        cargarEstudiantes();


    }

    private void cargarEstudiantes() {
        String url = "http://backend-posts.herokuapp.com/student";
        jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonArrayRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("json", error.getMessage());
    }

    @Override
    public void onResponse(JSONArray response) {
        try {
            for (int i = 0; i<response.length(); i++){
                Estudiante estudiante = new Estudiante();
                JSONObject jsonObject = response.getJSONObject(i);
                estudiante.set_id(jsonObject.getString("_id"));
                estudiante.setCedula(jsonObject.getString("cedula"));
                estudiante.setCiudad(jsonObject.getString("ciudad"));
                estudiante.setPais(jsonObject.getString("pais"));
                estudiante.setDescripcion(jsonObject.getString("descripcion"));
                estudiante.setNombre(jsonObject.getString("nombre"));
                estudiante.setApellido(jsonObject.getString("apellido"));
                listEstudiante.add(estudiante);
            }
            AdaptadorEstudiante adaptadorEstudiante = new AdaptadorEstudiante(listEstudiante);
            rvUsuarios.setAdapter(adaptadorEstudiante);


        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "No se pudo cargar", Toast.LENGTH_SHORT).show();
        }

    }
}
