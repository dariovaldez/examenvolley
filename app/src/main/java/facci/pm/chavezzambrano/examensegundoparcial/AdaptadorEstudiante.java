package facci.pm.chavezzambrano.examensegundoparcial;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdaptadorEstudiante
        extends RecyclerView.Adapter<AdaptadorEstudiante.EstudiantesViewHolder> {

    List<Estudiante> listaEstudiantes;
    private View.OnClickListener listener;

    public AdaptadorEstudiante(List<Estudiante> listaEstudiantes) {
        this.listaEstudiantes = listaEstudiantes;
    }


    @Override
    public AdaptadorEstudiante.EstudiantesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_estudiante, parent, false);
        return new EstudiantesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdaptadorEstudiante.EstudiantesViewHolder holder, int position) {
        holder.tv_id.setText("ID: " + listaEstudiantes.get(position).get_id());
        holder.tv_cedula.setText("CEDULA: " +listaEstudiantes.get(position).getCedula());
        holder.tv_ciudad.setText("CIUDAD: " +listaEstudiantes.get(position).getCiudad());
        holder.tv_pais.setText("PAIS: " +listaEstudiantes.get(position).getPais());
        holder.tv_descripcion.setText("DESCRIPCION: " +listaEstudiantes.get(position).getDescripcion());
        holder.tv_nombre.setText("NOMBRE: " +listaEstudiantes.get(position).getNombre());
        holder.tv_apellido.setText("APELLIDO: " +listaEstudiantes.get(position).getApellido());
    }

    @Override
    public int getItemCount() {
        return listaEstudiantes.size();
    }

    public class EstudiantesViewHolder extends RecyclerView.ViewHolder {

        TextView tv_id, tv_cedula, tv_ciudad, tv_pais, tv_descripcion, tv_nombre, tv_apellido;

        public EstudiantesViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_id = itemView.findViewById(R.id.tv_id);
            tv_cedula = itemView.findViewById(R.id.tv_cedula);
            tv_ciudad = itemView.findViewById(R.id.tv_ciudad);
            tv_pais = itemView.findViewById(R.id.tv_pais);
            tv_descripcion = itemView.findViewById(R.id.tv_descripcion);
            tv_nombre = itemView.findViewById(R.id.tv_nombre);
            tv_apellido = itemView.findViewById(R.id.tv_apellido);

        }
    }
}
